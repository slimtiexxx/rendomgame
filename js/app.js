var stageWidth = 600;
var stageHeight = 400;
var gameScore = 0;

Crafty.init(1000, 1000, document.getElementById('crafty-game'));

var game_assets = {
    "sprites": {
        "family_guy.png": {
            tile: 82,
            tileh: 120,
            map: {
                hero_idle: [0, 0],
                hero_walking: [0, 1],
                hero_alert: [0, 3],
                hero_jumping: [2, 3],
                hero_sitting: [0, 4]
            }
        }
    }
};

Crafty.load(game_assets);

var walking_hero = Crafty.e('2D, Canvas, hero_walking, SpriteAnimation, Twoway')
    .attr({
        x: 260,
        y: 40
    })
    .twoway(100);

walking_hero.reel("walking", 1000, [
    [0, 1],
    [1, 1],
    [2, 1],
    [3, 1],
    [4, 1],
    [5, 1],
    [6, 1]
]);

// var jumping_hero = Crafty.e('2D, Canvas, hero_alert, Keyboard, SpriteAnimation').attr({
//     x: 260,
//     y: 240
// });
//
// jumping_hero.reel("jumping", 1000, [
//     [0, 3],
//     [1, 3],
//     [2, 3],
//     [1, 3],
//     [0, 3]
// ]);
//
// jumping_hero.bind('KeyDown', function(evt) {
//     if (evt.key == Crafty.keys.UP_ARROW) {
//         jumping_hero.animate("jumping", 1);
//     }
// });

walking_hero.bind('KeyDown', function(evt) {
    if (evt.key == Crafty.keys.RIGHT_ARROW) {
        walking_hero.animate("walking", 1);
    }
});

walking_hero.bind('KeyUp', function(evt) {
    if (evt.key == Crafty.keys.RIGHT_ARROW) {
        walking_hero.animate("walking", 0);
    }
});